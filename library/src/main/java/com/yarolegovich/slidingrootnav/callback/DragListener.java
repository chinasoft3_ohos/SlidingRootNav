package com.yarolegovich.slidingrootnav.callback;

/**
 * Created by yarolegovich on 25.03.2017.
 */

public interface DragListener {
    /**
     * onDrag
     *
     * @param progress
     */
    void onDrag(float progress);
}
