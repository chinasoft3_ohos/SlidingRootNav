package com.yarolegovich.slidingrootnav.callback;

/**
 * Created by yarolegovich on 25.03.2017.
 */

public interface DragStateListener {
    /**
     * onDragStart
     */
    void onDragStart();

    /**
     * onDragEnd
     *
     * @param isMenuOpened
     */
    void onDragEnd(boolean isMenuOpened);
}
