package com.yarolegovich.slidingrootnav;

/**
 * Created by yarolegovich on 25.03.2017.
 */

public enum SlideGravity {
    LEFT,
    RIGHT
}
