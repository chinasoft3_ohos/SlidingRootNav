package com.yarolegovich.slidingrootnav;

/**
 * Created by yarolegovich on 25.03.2017.
 */

public interface SlidingRootNav {

    /**
     * isMenuClosed
     *
     * @return boolean
     */
    boolean isMenuClosed();

    /**
     * isMenuOpened
     *
     * @return boolean
     */
    boolean isMenuOpened();

    /**
     * isMenuLocked
     *
     * @return boolean
     */
    boolean isMenuLocked();

    /**
     * closeMenu
     */
    void closeMenu();

    /**
     * closeMenu
     *
     * @param animated
     */
    void closeMenu(boolean animated);

    /**
     * openMenu
     */
    void openMenu();

    /**
     * openMenu
     *
     * @param animated
     */
    void openMenu(boolean animated);

    /**
     * setMenuLocked
     *
     * @param locked
     */
    void setMenuLocked(boolean locked);

    /**
     * getLayout
     *
     * @return
     */
    SlidingRootNavLayout getLayout();

}
