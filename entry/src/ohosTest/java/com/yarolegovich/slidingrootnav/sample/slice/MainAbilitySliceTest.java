package com.yarolegovich.slidingrootnav.sample.slice;

import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;
import com.yarolegovich.slidingrootnav.SlidingRootNavLayout;
import com.yarolegovich.slidingrootnav.sample.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.DirectionalLayout;
import ohos.app.dispatcher.TaskDispatcher;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

public class MainAbilitySliceTest {
    static SlidingRootNav slidingRootNav;

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.yarolegovich.slidingrootnav.sample", actualBundleName);
    }

    @BeforeClass
    public static void initTest() {
        Ability ability = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        AbilitySlice slice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);

        TaskDispatcher taskDispatcher = slice.getMainTaskDispatcher();
        taskDispatcher.syncDispatch(new Runnable() {
            @Override
            public void run() {
                DirectionalLayout layout = new DirectionalLayout(slice);
                layout.setWidth(-1);
                layout.setHeight(-1);
                layout.setId(365);
                slice.setUIContent(layout);

                slidingRootNav = new SlidingRootNavBuilder(ability)
                        .withArbitrarilyView(ability.findComponentById(365))
                        .withMenuLayout(ResourceTable.Layout_menu_left_drawer)
                        .withRootViewScale(0.5f)
                        .withDragDistancePx(100)
                        .withOffsetDistancePx(200)
                        .inject();
            }
        });
    }

    @Test
    public void testIsClosed() {
        Assert.assertTrue(slidingRootNav.isMenuClosed());
    }

    @Test
    public void testIsLocked() {
        Assert.assertFalse(slidingRootNav.isMenuLocked());
    }

    @Test
    public void testIsOpened() {
        Assert.assertFalse(slidingRootNav.isMenuOpened());
    }

    @Test
    public void testLayout() {
        Assert.assertNotNull(slidingRootNav.getLayout());
    }

    @Test
    public void testRootViewScale() {
        SlidingRootNavLayout slidingRootNavLayout = slidingRootNav.getLayout();

        //获得私有变量
        Class unitClass = slidingRootNavLayout.getClass();
        try {
            Field field = unitClass.getDeclaredField("layoutScale");
            field.setAccessible(true);         //解除私有限定
            Float val4 = (Float) field.get(slidingRootNavLayout);
            Assert.assertTrue(val4 == 0.5f);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testDragDistancePx() {
        SlidingRootNavLayout slidingRootNavLayout = slidingRootNav.getLayout();

        //获得私有变量
        Class unitClass = slidingRootNavLayout.getClass();
        try {
            Field field = unitClass.getDeclaredField("maxDragDistance");
            field.setAccessible(true);         //解除私有限定
            int val4 = (int) field.get(slidingRootNavLayout);
            Assert.assertTrue(val4 == 100);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testOffsetDistancePx() {
        SlidingRootNavLayout slidingRootNavLayout = slidingRootNav.getLayout();

        //获得私有变量
        Class unitClass = slidingRootNavLayout.getClass();
        try {
            Field field = unitClass.getDeclaredField("offsetDistance");
            field.setAccessible(true);         //解除私有限定
            int val4 = (int) field.get(slidingRootNavLayout);
            Assert.assertTrue(val4 == 200);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}