/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.yarolegovich.slidingrootnav.sample.slice;

import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;
import com.yarolegovich.slidingrootnav.SlidingRootNavLayout;
import com.yarolegovich.slidingrootnav.callback.DragStateListener;
import com.yarolegovich.slidingrootnav.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.io.InputStream;

/**
 * MainAbilitySlice
 *
 * @author 赵俊
 * @since 2021-04-14
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private SlidingRootNav slidingRootNav;
    private Text textShow;
    private Text textItem1;
    private Text textItem2;
    private Text textItem3;
    private Text textItem4;
    private Text textItem5;
    private Button btnSwitch;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        textShow = (Text) findComponentById(ResourceTable.Id_text_show);
        btnSwitch = (Button) findComponentById(ResourceTable.Id_btn_switch);
        init();
        initLeftMenu();
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_switch:
                if (slidingRootNav.isMenuClosed()) {
                    slidingRootNav.openMenu();
                } else {
                    slidingRootNav.closeMenu();
                }
                break;
            case ResourceTable.Id_text_item_1:
                textShow.setText("Dashboard");
                setClickEffect((Text) component);
                break;
            case ResourceTable.Id_text_item_2:
                textShow.setText("My Account");
                setClickEffect((Text) component);
                break;
            case ResourceTable.Id_text_item_3:
                textShow.setText("Messages");
                setClickEffect((Text) component);
                break;
            case ResourceTable.Id_text_item_4:
                textShow.setText("Cart");
                setClickEffect((Text) component);
                break;
            case ResourceTable.Id_text_item_5:
                setClickEffect((Text) component);
                terminateAbility();
                break;
            default:
                break;
        }
    }

    /**
     * 初始化
     */
    public void init() {
        slidingRootNav = new SlidingRootNavBuilder(getAbility())
                .withArbitrarilyView(findComponentById(ResourceTable.Id_btn_switch))
                .withMenuLayout(ResourceTable.Layout_menu_left_drawer)
                .inject();

        btnSwitch.setClickedListener(this);

        SlidingRootNavLayout slidingRootNavLayout = slidingRootNav.getLayout();
        slidingRootNavLayout.addDragStateListener(new DragStateListener() {
            @Override
            public void onDragStart() {

            }

            @Override
            public void onDragEnd(boolean isMenuOpened) {
                if (isMenuOpened) {
                    btnSwitch.setBackground(new PixelMapElement(
                            getPixelMapFromResource(getContext(), ResourceTable.Media_return)));
                } else {
                    btnSwitch.setBackground(new PixelMapElement(
                            getPixelMapFromResource(getContext(), ResourceTable.Media_menu)));
                }
            }
        });
    }

    /**
     * 初始化左侧菜单
     */
    public void initLeftMenu() {
        textItem1 = (Text) slidingRootNav.getLayout().findComponentById(ResourceTable.Id_text_item_1);
        textItem2 = (Text) slidingRootNav.getLayout().findComponentById(ResourceTable.Id_text_item_2);
        textItem3 = (Text) slidingRootNav.getLayout().findComponentById(ResourceTable.Id_text_item_3);
        textItem4 = (Text) slidingRootNav.getLayout().findComponentById(ResourceTable.Id_text_item_4);
        textItem5 = (Text) slidingRootNav.getLayout().findComponentById(ResourceTable.Id_text_item_5);
        textItem1.setClickedListener(this);
        textItem2.setClickedListener(this);
        textItem3.setClickedListener(this);
        textItem4.setClickedListener(this);
        textItem5.setClickedListener(this);

        textShow.setText("Dashboard");
        setClickEffect(textItem1);
    }

    /**
     * 设置点击效果
     *
     * @param text
     */
    public void setClickEffect(Text text) {
        if (textItem1 == null) {
            return;
        }
        String color = "#223853";
        textItem1.setTextColor(new Color(Color.getIntColor(color)));
        textItem2.setTextColor(new Color(Color.getIntColor(color)));
        textItem3.setTextColor(new Color(Color.getIntColor(color)));
        textItem4.setTextColor(new Color(Color.getIntColor(color)));
        textItem5.setTextColor(new Color(Color.getIntColor(color)));
        text.setTextColor(new Color(Color.getIntColor("#72B4D0")));
        slidingRootNav.closeMenu();
    }

    /**
     * 通过资源ID获取位图对象
     *
     * @param context
     * @param resourceId
     * @return PixelMap
     */
    private PixelMap getPixelMapFromResource(Context context, int resourceId) {
        InputStream inputStream = null;
        try {
            // 创建图像数据源ImageSource对象
            inputStream = context.getResourceManager().getResource(resourceId);
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/jpg";
            ImageSource imageSource = ImageSource.create(inputStream, srcOpts);
            // 设置图片参数
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            return imageSource.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }
}
