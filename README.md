﻿# SlidingRootNav

#### 项目介绍
- 项目名称：SlidingRootNav
- 所属系列：openharmony的第三方组件适配移植
- 功能：一种类似于抽屉布局，并且可以使其“抽屉”隐藏在“内容”视图下，如果移动内容后可以显示底部抽屉
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v1.1.1

#### 效果演示
![image text](gif/demo.gif)

#### 安装教程
1.在项目根目录下的build.gradle文件中，
```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```
dependencies {
   implementation('com.gitee.chinasoft_ohos:SlidingRootNav:1.0.0')
   ......  
}
```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


#### 使用说明
定义SlidingRootNav，并且使用SlidingRootNavBuilder中inject方法进行实例化。SlidingRootNavBuilder中会有一些初始化配置的方法。
```java
SlidingRootNav slidingRootNav = new SlidingRootNavBuilder(getAbility())
    .withArbitrarilyView(findComponentById(ResourceTable.Id_btn_switch))
    .withMenuLayout(ResourceTable.Layout_menu_left_drawer)
    .withDragDistance(150)
    .withRootViewScale(0.7f) 
    .inject();
```
此处注意，必须调用方法withArbitrarilyView，该方法传入一个当前界面中，任意一个控件ID。
SlidingRootNav方法中定义了启动布局，关闭布局等方法
```java
if (slidingRootNav.isMenuClosed()) {
	slidingRootNav.openMenu();
} else {
	slidingRootNav.closeMenu();
}
```
SlidingRootNav包含的接口如下：
```java
public interface SlidingRootNav {
    boolean isMenuClosed();
    boolean isMenuOpened();
    boolean isMenuLocked();
    void closeMenu();
    void closeMenu(boolean animated);
    void openMenu();
    void openMenu(boolean animated);
    void setMenuLocked(boolean locked);
    SlidingRootNavLayout getLayout(); //If for some reason you need to work directly with layout - you can
}
```
还可以增加接口回调，如：
Drag progress:
```java
builder.addDragListener(listener);

public interface DragListener {
  void onDrag(float progress); //Float between 0 and 1, where 1 is a fully visible menu
}
```
Drag state changes:
```java
builder.addDragStateListener(listener);

public interface DragStateListener {
  void onDragStart();
  void onDragEnd(boolean isMenuOpened);
}
```

#### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息

```
Copyright 2017 Yaroslav Shevchuk

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```